package com.example.mylaw.task3;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

/**
 * Created by mylaw on 30.10.16.
 */

public class BmiActivity extends AppCompatActivity {

    public static final String META = "METADANE";

    Double weight, height;
    EditText textWeight, textHeight;
    TextView textLabelHeight, textLabelWeight, textBmiResult;
    Button bttnBmi;

        // alt+enter w celu szybkiego zaimportowania potrzebnych bibliotek


    public void setBmi(){
        weight = Double.parseDouble(textWeight.getText().toString());
        height = Double.parseDouble(textHeight.getText().toString());
        height = height / 100;
        height = height * height; // zastąpić kwadratem
        double Bmi = weight / height;
        textBmiResult = (TextView) findViewById(R.id.textBmiResult);
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            String bmi = df.format(Bmi);
            textBmiResult.setText("Twoje Bmi wynosi: " + bmi);
            // CTRL + X do wycinania całej lini
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
        textBmiResult.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);
        View actBmi = (View) findViewById(R.id.activity_bmi);
        textWeight = (EditText) findViewById(R.id.textWeight);
        textHeight = (EditText) findViewById(R.id.textHeight);
        textLabelWeight = (TextView) findViewById(R.id.text_weight);
        textLabelHeight = (TextView) findViewById(R.id.text_height);
        textBmiResult = (TextView) findViewById(R.id.textBmiResult);

        SharedPreferences settings = getSharedPreferences(META, MODE_PRIVATE);
        Integer bgColor = settings.getInt("bgColor", Color.WHITE);
        Integer txtColor = settings.getInt("txtColor", Color.WHITE);
        actBmi.setBackgroundColor(bgColor);
        textWeight.setTextColor(txtColor);
        textHeight.setTextColor(txtColor);
//        textLabelWeight.setTextColor(txtColor);
//        textLabelHeight.setTextColor(txtColor);
//        textBmiResult.setTextColor(txtColor);


        bttnBmi = (Button) findViewById(R.id.bttnBmi);
        bttnBmi.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(textWeight.getText().toString().equals("") && textHeight.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Proszę podać swoją wagę i wzrost!", Toast.LENGTH_LONG).show();
                }
                else if(textWeight.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Proszę podać swoją wagę!", Toast.LENGTH_SHORT).show();
                }
                else if(textHeight.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Proszę podać swój wzrost!", Toast.LENGTH_SHORT).show();
                }
                else setBmi();
            }
        });
    }
}
