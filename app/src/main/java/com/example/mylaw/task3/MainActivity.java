package com.example.mylaw.task3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String META = "METADANE";

    Button buttonCalc, buttonBmi;
    Switch switchColor;
    View actMain, actBmi, actCalc;


    public void buttonClicked(View v) {
        String bttnName = getResources().getResourceEntryName(v.getId());
//        Toast.makeText(getApplicationContext(), getResources().getResourceEntryName(v.getId()), Toast.LENGTH_SHORT).show();
//        SharedPreferences settings = getSharedPreferences(META, MODE_PRIVATE);
//        SharedPreferences.Editor editor = settings.edit();
//        editor.putString("Context", v.getContext().toString());
//        editor.commit();
//        Toast.makeText(getApplicationContext(), bttnName, Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        if (bttnName.contains("bttnBmi")) {
            intent = new Intent(this, BmiActivity.class);
        } else if (bttnName.contains("bttnCalc")) {
            intent = new Intent(this, CalcActivity.class);
        }
        try {
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonCalc = (Button) findViewById(R.id.bttnCalc);
        buttonBmi = (Button) findViewById(R.id.bttnBmi);
        switchColor = (Switch) findViewById(R.id.switchColor);
        switchColor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                actMain = (RelativeLayout) findViewById(R.id.activity_main);
                SharedPreferences settings = getSharedPreferences(META, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();

                if (isChecked) {
                    actMain.setBackgroundColor(Color.BLACK);
                    editor.putInt("bgColor", Color.BLACK);
                    editor.putInt("txtColor", Color.WHITE);
//                    actBmi.setBackgroundColor(Color.BLACK);
//                    actCalc.setBackgroundColor(Color.BLACK);
                } else {
                    actMain.setBackgroundColor(Color.WHITE);
                    editor.putInt("bgColor", Color.WHITE);
                    editor.putInt("txtColor", Color.BLACK);
//                    actBmi.setBackgroundColor(Color.WHITE);
//                    actCalc.setBackgroundColor(Color.WHITE);
                }
                editor.commit();

            }
        });
    }
}
