package com.example.mylaw.task3;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Created by mylaw on 01.11.16.
 */

public class CalcActivity extends AppCompatActivity {

    private static final String META = "METADANE";
    private static final String TAG = "TASK3";

    private Button button0;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;
    private Button button10;
    private Button button11;
    private Button button12;
    private Button button13;
    private Button button14;
    private Button button15;
    private Button button16;
    private Button button17;
    private Button button18;
    private Button button19;

    public EditText textSum;
    public EditText textCalc;

    private double result = 0;

    private String tempSign;
    private String calc = "";
    private String[] calcArr;

    private Boolean isDecimal = Boolean.FALSE;
    private Boolean resultPressed = Boolean.FALSE;
    private Boolean isSqrt = Boolean.FALSE;

    private List<String> signLists = Arrays.asList("+", "-", "*", "/", "^", "√", "←", ".");


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calc);

        initControls();
    }


    private void initControls() {

        button0 = (Button) findViewById(R.id.button0);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        button6 = (Button) findViewById(R.id.button6);
        button7 = (Button) findViewById(R.id.button7);
        button8 = (Button) findViewById(R.id.button8);
        button9 = (Button) findViewById(R.id.button9);
        button10 = (Button) findViewById(R.id.button10);
        button11 = (Button) findViewById(R.id.button11);
        button12 = (Button) findViewById(R.id.button12);
        button13 = (Button) findViewById(R.id.button13);
        button14 = (Button) findViewById(R.id.button14);
        button15 = (Button) findViewById(R.id.button15);
        button16 = (Button) findViewById(R.id.button16);
        button17 = (Button) findViewById(R.id.button17);
        button18 = (Button) findViewById(R.id.button18);
        button19 = (Button) findViewById(R.id.button19);
        textSum = (EditText) findViewById(R.id.textSum);
        textCalc = (EditText) findViewById(R.id.textCalc);

        View actCalc = (View) findViewById(R.id.activity_calc);


        SharedPreferences settings = getSharedPreferences(META, MODE_PRIVATE);
        Integer bgColor = settings.getInt("bgColor", Color.WHITE);
        Integer txtColor = settings.getInt("txtColor", Color.WHITE);
        actCalc.setBackgroundColor(bgColor);
        textCalc.setTextColor(txtColor);
    }


    public void onClick(View v) {
        Button b = (Button) v;
        String chr = b.getText().toString();

        switch (chr) {
            case "=":
                resultPressed = Boolean.TRUE;
                calculate();
                break;

            case "C":
                clear();
                break;

            case "←":
                eraseLastChr();
                break;

            case ".":
                decimalValue();
                break;

            case "√":
                isSqrt = Boolean.TRUE;
                calculate();
                break;

            default:
                addSign(chr);
        }
//        Toast.makeText(getApplicationContext(), chr, Toast.LENGTH_SHORT).show();
    }


    public void addSign(String chr) {

        String lastItem = getLastItem();

        if (signLists.contains(chr)) {
            if (signLists.contains(lastItem))
                eraseLastChr();
            calc = calc + " " + chr + " ";
        } else
            calc = calc + chr;

        textCalc.setText(calc);
        calcArr = textCalc.getText().toString().split(" ");

        if (calcArr.length == 4) {
            tempSign = calcArr[3];
            calculate();
        }
    }


    public void calculate() {

        calcArr = textCalc.getText().toString().split(" ");
        DecimalFormat df = new DecimalFormat("#.##");
        String r;
        result = 0;

        if (tempSign != null)
            calcArr = Arrays.copyOf(calcArr, calcArr.length - 1);

        if (textCalc.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Brak możliwości obliczenia!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (calcArr.length == 1){
            if (isSqrt)
                result = Math.sqrt(Double.parseDouble(textCalc.getText().toString()));
            else if (!textSum.getText().toString().equals(""))
                result = Double.parseDouble(textSum.getText().toString());

            if (resultPressed) {
                Toast.makeText(getApplicationContext(), "Brak możliwości obliczenia!", Toast.LENGTH_SHORT).show();
                resultPressed = Boolean.FALSE;
                return;
            }
        }


        if (calcArr.length == 2) {
            Toast.makeText(getApplicationContext(), "Dodaj jakąś liczbę by obliczyć działanie!", Toast.LENGTH_LONG).show();
            return;
        }

        Log.v(TAG, Double.toString(result));

        if (calcArr.length == 1 || calcArr.length == 3)
            if (result == 0) {
                for (String str : calcArr) {
                    if (str.equals("*")) {
                        result = Double.parseDouble(calcArr[0]) * Double.parseDouble((calcArr[2]));
                    } else if (str.equals("/")) {
                        result = Double.parseDouble(calcArr[0]) / Double.parseDouble((calcArr[2]));
                    } else if (str.equals("+")) {
                        result = Double.parseDouble(calcArr[0]) + Double.parseDouble((calcArr[2]));
                    } else if (str.equals("-")) {
                        result = Double.parseDouble(calcArr[0]) - Double.parseDouble((calcArr[2]));
                    } else if (str.equals("^")) {
                        result = Math.pow(Double.parseDouble(calcArr[0]), Double.parseDouble(calcArr[2]));
                    }
                }
            }


        r = df.format(result);
        r = r.replace(",", ".");
        textSum.setText(r);

        if (isSqrt) {
            if (!textSum.getText().toString().equals(""))
                result = Math.sqrt(Double.parseDouble(textSum.getText().toString()));
        }

        r = df.format(result);
        r = r.replace(",", ".");
        textSum.setText(r);
        isSqrt = Boolean.FALSE;


        if (tempSign != null) {
            r += " " + tempSign + " ";
            tempSign = null;
        }

        textCalc.setText(r);
        calc = r;
        resultPressed = Boolean.FALSE;
    }


    public void clear() {

        textCalc.setText("");
        textSum.setText("");
        tempSign = null;
        isSqrt = Boolean.FALSE;
        isDecimal = Boolean.FALSE;
        result = 0;
        calc = "";
    }


    public void decimalValue() {

        calcArr = textCalc.getText().toString().split(" ");
        calc = textCalc.getText().toString();

        if (!calc.equals("")) {
            if (isDecimal) {
                if (calc.contains(".")) {
                    if (calcArr.length == 1) {
                        calc = calc.replace(".", "");
                        isDecimal = Boolean.FALSE;
                    } else if (calcArr.length == 3) {
                        calc = calc.replace(getLastItem(), "");
                        calc = calc + getLastItem().replace(".", "");
                        isDecimal = Boolean.FALSE;
                    }
                }
            } else {
                if (calcArr.length == 1) {
                    isDecimal = Boolean.TRUE;
                    calc = calc + ".";
                } else if (calcArr.length == 2) {
                    isDecimal = Boolean.TRUE;
                    calc = calc + "0.";
                } else if (calcArr.length == 3) {
                    isDecimal = Boolean.TRUE;
                    calc = calc + ".";
                }
            }
        } else {
                calc = "0.";
                isDecimal = Boolean.TRUE;
        }

        textCalc.setText(calc);
    }


    public String getLastItem(){

        calcArr = textCalc.getText().toString().split(" ");
        String last = calcArr[calcArr.length-1];
        return last;
    }

    public void eraseLastChr(){

        int i = 1;
//        Toast.makeText(getApplicationContext(), getLastItem(), Toast.LENGTH_SHORT).show();
        calcArr = textCalc.getText().toString().split(" ");
        calc = textCalc.getText().toString();
//        if((calcArr.length == 1 || calcArr.length == 3) && isDecimal && calc.contains("."))
//            calc = calc.substring(0, calc.length()-1);

        if(!calc.equals("")){
            if (signLists.contains(getLastItem()))
                i = 3;
            calc = calc.substring(0, calc.length()-i);
            textCalc.setText(calc);
        }
    }
}
